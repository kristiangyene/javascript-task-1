// DOM elements
const elLoanBtn = document.getElementById("loanBtn");
const elBankBtn = document.getElementById("bankBtn");
const elWorkBtn = document.getElementById("workBtn");
const elBuyBtn = document.getElementById("buyBtn");
const elEarnings = document.getElementById("earnings");
const elBalance = document.getElementById("balance");
const elComputers = document.getElementById("computers");
const elLaptopName = document.getElementById("laptopName");
const elFeatures = document.getElementById("features");
const elInfo = document.getElementById("info");
const elTotal = document.getElementById("totalPrice");
const elImage = document.getElementById("image");

// Variables
let balance = 0;
let earnings = 0;
let totalPrice = 0;
let loanCheck = false;
let laptopTitle = elLaptopName.innerText;
let info = "";
let features = "";
let image = "";
const computers = [
    {name: "Mactablet Pro", price: 1200, info: "Tilbyr en bærbar løsning for de daglige behovene dine. Denne lille bærbare PC-en er fleksibel nok til å brukes til alt.", features: "- Intel® Celeron N4000-processor\n- 4 GB DDR4 RAM\n- 64 GB flash", image: "https://www.elkjop.no/image/dv_web_D180001002461058/161658/macbook-air-2020-133-256-gb-gull.jpg?$fullsize$"},
    {name: "HP Flybook", price: 2500, info: "Et rent og enkelt design som utstråler kvalitet, det er flybook. Perfekt for studenter.", features: "- Intel® Celeron N4000-processor\n-14 HD-screen\n-32 GB flash", image: "https://www.elkjop.no/image/dv_web_D180001002448639/167021/huawei-matebook-13-2020-i58gbmx250-13-baerbar-pc.jpg?$fullsize$"},
    {name: "Asys Interoon", price: 5000, info: "Kombinerer en svært lett vekt og portabilitet med en høytytende brikke som leverer avansert ytelse under krevende oppgaver.", features: "- Intel® Core i7-prosessor\n- Nvidia GeForce 4 GB\n- 16 GB RAM", image: "https://www.elkjop.no/image/dv_web_D180001002468136/181551/samsung-galaxy-book-ion-133-baerbar-pc-aura-silver.jpg?$fullsize$"},
    {name: "Acar Air", price: 700, info: "Svært lettvektige aluminiumskabinett er den ideelle\nhverdagsmaskinen.", features: "- Intel® Core i7-prosessor\n- 13 1440p touchscreen\n- Nvidia GeForce 2 GB", image: "https://www.elkjop.no/image/dv_web_D180001002361727/105536/acer-chromebook-314-acnxhkded001-14-baerbar-pc-soelv.jpg?$fullsize$"}
];

// Setting options for computer dropdown
computers.forEach((computer) => {
    var option = document.createElement("option");
    option.value = computer.name;
    option.text = computer.name;
    elComputers.appendChild(option);
});

// Listeners
elLoanBtn.addEventListener("click", function(){
    const loan = parseInt(prompt("Please enter the amout of loan you wish for.\nRequirements:\n \
    -You can not get a loan more than double of your bank balance. \n \
    -You can not get more than one loan before buying a computer.", 0));
    if(!loan) alert("Loan cancelled."); //User clicks cancel
    else if((loan > balance * 2) || loanCheck) alert("Oops! Something wrong happened with your loan.");
    else{ 
        balance += loan;
        loanCheck = true;
        render();
    }
});

elBankBtn.addEventListener("click", function(){
    console.log("Transfer to bank");
    balance += earnings;
    earnings = 0;
    render();
});

elWorkBtn.addEventListener("click", function(){
    console.log("Worked some hours");
    earnings += 100;
    render();
});

elComputers.addEventListener("change", function(){
    console.log("Change selection");
    const computer = computers.find(el => el.name == elComputers.value);
    laptopTitle = computer.name;
    totalPrice = computer.price;
    features = "Features:\n" + computer.features;
    info = computer.info;
    image = computer.image;
    render();
});

elBuyBtn.addEventListener("click", function(){
    if(balance < totalPrice) alert("Not enough money to buy this computer.");
    else{
        console.log("Laptop bought");
        balance -= totalPrice;
        loanCheck = false;
        alert("Thank you for your purchase.");
        render();
    }
});

// Render used after events
function render(){
    elBalance.value = "Balance:\t" + balance + " kr.";
    elEarnings.value = "Pay:\t" + earnings + " kr.";
    elTotal.value = totalPrice + " kr.";
    elLaptopName.innerText = laptopTitle;
    elFeatures.innerText = features;
    elInfo.innerText = info;
    elImage.src = image;
    

}


